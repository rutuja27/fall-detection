package com.example.fall_detection_device;

public class test {

    public static void main(String args[])
    {
        double a = Math.PI;

        // get a variable x which is equal to PI/2
        double x = Math.PI / 2;

        // convert x to radians
        x = Math.toRadians(x);

        // get the arc cosine of x
        System.out.println("Math.acos(" + x + ")=" + Math.acos(x));
    }
}
