package com.example.fall_detection_device;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.*;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    Sensor accelerometer;

    private static final int ACCELEROMETER_SAMPLING_PERIOD = 1000000;
    private static final double CSV_THRESHOLD = 23;
    private static final double CAV_THRESHOLD = 18;
    private static final double CCA_THRESHOLD = 65.5;

    private Long lastSentInMillis;
    private Long minTimeToNotifyAgain = 30000L;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private List<Map<AccelerometerAxis, Double>> accelerometerValues = new ArrayList<>();
    TextView res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(MainActivity.this, accelerometer, 32000);
        res = (TextView) findViewById(R.id.fall);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        //double loX = event.values[0];
        //double loY = event.values[1];
        //double loZ = event.values[2];

        double x = event.values[0];
        double y = event.values[1];
        double z = event.values[2];

        if (this.isFallDetected(x, y, z)) {
            if (this.isOkayToNotifyAgain()) {
                this.lastSentInMillis = System.currentTimeMillis();
                //Toast.makeText(this, super.getString(R.string.fall_happened), Toast.LENGTH_LONG).show();
                //System.out.println("Fall Happend..!!");
                //FallNotificationActivity.startFallNotificationActivity(this, this.gson.toJson(this.user));
            }
        }
    }

    private boolean isFallDetected(double x,
                                   double y,
                                   double z) {

        double acceleration = this.calculateSumVector(x, y, z);
        this.addAccelerometerValuesToList(x, y, z, acceleration);

        StringBuilder msg = new StringBuilder("x: ").append(x)
                .append(" y: ").append(y)
                .append(" z: ").append(z)
                .append(" acc: ").append(acceleration);
        //Log.d("FDS-Acc-Values", msg.toString());
        res.setText(msg.toString());


        if (acceleration > CSV_THRESHOLD) {
            double angleVariation = this.calculateAngleVariation();
            if (angleVariation > CAV_THRESHOLD) {
                double changeInAngle = this.calculateChangeInAngle();
                if (changeInAngle > CCA_THRESHOLD) {
                    msg.append(System.currentTimeMillis());
                    //Log.d("FDS-Fall-Happened", msg.toString());
                    openDialog("Fall Detected..!!");
                    System.out.println("Fall Detected..!!!!================================================");
                    return true;
                }
            }

        }
        return false;
    }

    public void openDialog(String message)
    {
        AlertDialog.Builder a_builder = new AlertDialog.Builder(MainActivity.this);
        a_builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = a_builder.create();
        alert.setTitle("");
        alert.show();
    }

    private void addAccelerometerValuesToList(double x,
                                              double y,
                                              double z,
                                              double acceleration) {
        if (this.accelerometerValues.size() >= 4) {
            this.accelerometerValues.remove(0);
        }
        Map<AccelerometerAxis, Double> map = new HashMap<>();
        map.put(AccelerometerAxis.X, x);
        map.put(AccelerometerAxis.Y, y);
        map.put(AccelerometerAxis.Z, z);
        map.put(AccelerometerAxis.ACCELERATION, acceleration);
        this.accelerometerValues.add(map);
    }

    private double calculateSumVector(double x,
                                      double y,
                                      double z) {
        return Math.abs(x) + Math.abs(y) + Math.abs(z);
    }

    private double calculateAngleVariation() {
        int size = this.accelerometerValues.size();
        if (size < 2) {
            return -1;
        }

        Map<AccelerometerAxis, Double> minusTwo = this.accelerometerValues.get(size - 2);
        Map<AccelerometerAxis, Double> minusOne = this.accelerometerValues.get(size - 1);

        double anX = minusTwo.get(AccelerometerAxis.X) * minusOne.get(AccelerometerAxis.X);
        double anY = minusTwo.get(AccelerometerAxis.Y) * minusOne.get(AccelerometerAxis.Y);
        double anZ = minusTwo.get(AccelerometerAxis.Z) * minusOne.get(AccelerometerAxis.Z);
        double an = anX + anY + anZ;

        double anX0 = Math.pow(minusTwo.get(AccelerometerAxis.X), 2);
        double anY0 = Math.pow(minusTwo.get(AccelerometerAxis.Y), 2);
        double anZ0 = Math.pow(minusTwo.get(AccelerometerAxis.Z), 2);
        double an0 = Math.sqrt(anX0 + anY0 + anZ0);

        double anX1 = Math.pow(minusOne.get(AccelerometerAxis.X), 2);
        double anY1 = Math.pow(minusOne.get(AccelerometerAxis.Y), 2);
        double anZ1 = Math.pow(minusOne.get(AccelerometerAxis.Z), 2);
        double an1 = Math.sqrt(anX1 + anY1 + anZ1);

        double a = an / (an0 * an1);

        return Math.acos(a) * (180 / Math.PI);
    }

    private double calculateChangeInAngle() {
        int size = this.accelerometerValues.size();
        if (size < 4) {
            return -1;
        }
        Map<AccelerometerAxis, Double> first = this.accelerometerValues.get(0);
        Map<AccelerometerAxis, Double> third = this.accelerometerValues.get(3);

        double aX = first.get(AccelerometerAxis.X) * third.get(AccelerometerAxis.X);
        double aY = first.get(AccelerometerAxis.Y) * third.get(AccelerometerAxis.Y);
        double aZ = first.get(AccelerometerAxis.Z) * third.get(AccelerometerAxis.Z);

        double a0 = aX + aY + aZ;

        aX = Math.pow(aX, 2);
        aY = Math.pow(aY, 2);
        aZ = Math.pow(aZ, 2);
        double a1 = (Math.sqrt(aX) + Math.sqrt(aY) + Math.sqrt(aZ));

        return Math.acos(a0 / a1) * (180 / Math.PI);
    }

    private boolean isOkayToNotifyAgain() {
        return this.lastSentInMillis == null || (this.lastSentInMillis + this.minTimeToNotifyAgain) < System.currentTimeMillis();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
