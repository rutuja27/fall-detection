package com.example.fall_detection_device;

enum AccelerometerAxis {

    X,
    Y,
    Z,
    ACCELERATION
}
