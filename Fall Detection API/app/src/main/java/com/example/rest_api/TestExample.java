package com.example.rest_api;

import java.io.*;
import java.util.*;

public class TestExample
{
    static Queue<Object> queue_sensor = new LinkedList<>();
    static List<Integer> arrayList = new ArrayList<Integer>();
    static Object final_result;

    public static void main(String args[])throws IOException
    {

        for (int i = 0; i < 10000; i++)
        {
            arrayList.add(i);
        }

        int size = 25;
        for (int start = 0; start < arrayList.size(); start += size) {
            int end = Math.min(start + size, arrayList.size());
            List<Integer> sublist = arrayList.subList(start, end);
            //System.out.println(sublist);
            queue_sensor.add(sublist);
        }
        FIFO();
    }

    public static void FIFO()
    {
        while (queue_sensor.size() != 0)
        {
            final_result = queue_sensor.remove();
            System.out.println(final_result);
            System.out.println(queue_sensor.size());
        }
    }
}
