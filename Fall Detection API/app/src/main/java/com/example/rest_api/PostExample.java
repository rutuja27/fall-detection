package com.example.rest_api;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PostExample {

    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    OkHttpClient client = new OkHttpClient();

    String post(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(json, JSON);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    String inputs(String sensor_data) {
        return "{\"inputs\":[[[1.355545, -0.15792847, 9.756744], [1.3531494, -0.16390991, 9.76033], [1.3531494, -0.16390991, 9.76033], [1.3567352, -0.1651001, 9.756744], [1.355545, -0.15792847, 9.757935], [1.355545, -0.15792847, 9.754349], [1.360321, -0.16390991, 9.755539], [1.3591309, -0.16151428, 9.759125], [1.3591309, -0.16271973, 9.751953], [1.355545, -0.16270447, 9.759125], [1.355545, -0.16270447, 9.759125], [1.3567352, -0.16511536, 9.7495575], [1.351944, -0.16151428, 9.756744], [1.3507538, -0.16271973, 9.750763], [1.3579254, -0.15911865, 9.757935], [1.3531494, -0.16390991, 9.751953], [1.3615265, -0.16271973, 9.755539], [1.3579254, -0.16271973, 9.753143], [1.3507538, -0.16749573, 9.755539], [1.3531494, -0.16870117, 9.750763], [1.360321, -0.1603241, 9.748352], [1.355545, -0.15911865, 9.748367], [1.3543396, -0.16151428, 9.753143], [1.3579254, -0.16271973, 9.753143], [1.3483582, -0.16749573, 9.75914]]]}";
    }

    String response()
    {
        PostExample example = new PostExample();
        String sensor_data = "";
        //String json = object.input_data(sensor_data);
        String json = example.inputs(sensor_data);
        String response = null;
        try {
            response = example.post("http://lb-fall-detection-api-3856896.us-west-2.elb.amazonaws.com/predict", json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(response);
        return response;

    }

    public static void main(String[] args) throws IOException {
        //PostExample example = new PostExample();
        //example.response();
        //System.out.println(response);
    }
}
