package com.example.rest_api;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.*;
import org.json.JSONObject;

import okhttp3.MediaType;

import java.net.*;
import java.io.*;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private static final String TAG = "MainActivity";
    private SensorManager sensorManager;
    Sensor accelerometer;
    Button go;
    TextView res;


    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private float x;
    private float y;
    private float z;

    long mlPreviousTime;
    int i;
    String result;

    AlertDialog.Builder alertDialog;
    static Object POST_PARAMS;
    static List<String> arrayList = new ArrayList<String>();
    Queue<Object> queue_sensor =  new LinkedList<>();
    String final_result;

    Queue<Object> queue1 = new PriorityQueue<>();
    int len = 25;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        go = findViewById(R.id.go);
        res = (TextView) findViewById(R.id.myAwesomeTextView);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(MainActivity.this, accelerometer, 32000);


        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                //getDataFIFO();
                //PassDATA();

            }
        });

    }

    public void getDataFIFO()
    {
        new Thread()
        {
            public void run()
            {
                while(true)
                {
                    try
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run()
                            {
                                queue1.add(final_result);
                                POST_PARAMS = queue1;
                                int size = queue1.size();
                                //System.out.println(size);
                                if(size == len)
                                {
                                    result = "{\"inputs\":["+POST_PARAMS+"]}";
                                    System.out.println(result);
                                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                                    StrictMode.setThreadPolicy(policy);
                                    try
                                    {
                                        String urlAdress = "http://lb-fall-detection-api-3856896.us-west-2.elb.amazonaws.com/predict";
                                        URL obj = new URL(urlAdress);
                                        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                                        con.setRequestMethod("POST");
                                        con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                                        con.setRequestProperty("Accept", "application/json");
                                        con.setDoOutput(true);

                                        OutputStream os = con.getOutputStream();
                                        os.write(result.getBytes());
                                        os.flush();
                                        os.close();
                                        int responseCode = con.getResponseCode();

                                        if (responseCode == HttpURLConnection.HTTP_OK)
                                        {
                                            BufferedReader in = new BufferedReader(new InputStreamReader(
                                                    con.getInputStream()));
                                            String inputLine;
                                            StringBuffer response = new StringBuffer();

                                            while ((inputLine = in.readLine()) != null) {
                                                response.append(inputLine);
                                            }
                                            in.close();

                                            res.setText(response.toString());
                                            String tmpStr = String.valueOf(response);
                                            String newStr = tmpStr.substring(1, tmpStr.length()-1);
                                            String jsonStr = newStr;
                                            JSONObject jsonObj = new JSONObject(jsonStr);
                                            String name = jsonObj.getString("prediction");
                                            if(name.equals("Fall Detected"))
                                            {
                                                System.out.println(name);
                                                //openDialog(name);
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        //e.printStackTrace();
                                    }
                                    for (int j =0;j<len;j++)
                                    {
                                        queue1.poll();
                                    }
                                }
                            }
                        });
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        /*float[] values = event.values;
        x = values[0];
        y = values[1];
        z = values[2];*/

        double loX = event.values[0];
        double loY = event.values[1];
        double loZ = event.values[2];

        /*double loAccelerationReader = Math.sqrt(Math.pow(loX, 2)
                + Math.pow(loY, 2)
                + Math.pow(loZ, 2));
        mlPreviousTime = System.currentTimeMillis();
        //Log.i(TAG, "loX : " + loX + " loY : " + loY + " loZ : " + loZ);
        boolean moIsMin = false;
        if (loAccelerationReader <= 6.0) {
            moIsMin = true;
            Log.i(TAG, "min");
        }

        boolean moIsMax = false;
        if (moIsMin) {
            i++;
            Log.i(TAG, " loAcceleration : " + loAccelerationReader);
            if (loAccelerationReader >= 30) {
                long llCurrentTime = System.currentTimeMillis();
                long llTimeDiff = llCurrentTime - mlPreviousTime;
                Log.i(TAG, "loTime :" + llTimeDiff);
                if (llTimeDiff >= 10) {
                    moIsMax = true;
                    Log.i(TAG, "max");
                }
            }

        }

        if (moIsMin && moIsMax) {
            //Log.i(TAG, "loX : " + loX + " loY : " + loY + " loZ : " + loZ);
            Log.i(TAG, "FALL DETECTED!!!!!");
            Toast.makeText(this, "FALL DETECTED!!!!!", Toast.LENGTH_LONG).show();
            i = 0;
            moIsMin = false;
            moIsMax = false;
        }

        if (i > 5) {
            i = 0;
            moIsMin = false;
            moIsMax = false;
        }*/

       final_result = "["+loX +", "+ loY + ", "+ loZ+"]";
       //System.out.println(final_result);
       //arrayList.add(final_result);
       /*int size = 25;
       for (int start = 0; start < arrayList.size(); start += size)
       {
            int end = Math.min(start + size, arrayList.size());
            List<String> sublist = arrayList.subList(start, end);
            //System.out.println(sublist.size());
            queue_sensor.add(String.valueOf(sublist));
       }
       FIFO();*/
       FIFO1();
    }

    public void FIFO1()
    {
        queue1.add(final_result);
        POST_PARAMS = queue1;
        int size = queue1.size();
        //System.out.println(size);
        if(size == len)
        {
            result = "{\"inputs\":[" + POST_PARAMS + "]}";
            //System.out.println(result);
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            try
            {
                String urlAdress = "http://lb-fall-detection-api-3856896.us-west-2.elb.amazonaws.com/predict";
                URL obj = new URL(urlAdress);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                con.setRequestProperty("Accept", "application/json");
                con.setDoOutput(true);

                OutputStream os = con.getOutputStream();
                os.write(result.getBytes());
                os.flush();
                os.close();
                int responseCode = con.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK)
                {
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();

                    res.setText(response.toString());
                    String tmpStr = String.valueOf(response);
                    String newStr = tmpStr.substring(1, tmpStr.length() - 1);
                    String jsonStr = newStr;
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String name = jsonObj.getString("prediction");
                    String percentage = jsonObj.getString("prediction_confidence");
                    if (name.equals("Fall Detected"))
                    {
                        System.out.println(name);
                        System.out.println(percentage);
                        openDialog(name + "   " +percentage);
                    }
                }
            } catch (Exception e)
            {
                //e.printStackTrace();
            }
            for (int j = 0; j < len; j++)
            {
                queue1.poll();
            }
        }
    }

    public void openDialog(String message)
    {
        AlertDialog.Builder a_builder = new AlertDialog.Builder(MainActivity.this);
        a_builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = a_builder.create();
        alert.setTitle("Alert..!!");
        alert.show();
    }

    public void FIFO()
    {
        while (queue_sensor.size() != 0)
        {
            POST_PARAMS = queue_sensor.remove();
            result = "{\"inputs\":["+POST_PARAMS+"]}";
            System.out.println(result);
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            try
            {
                String urlAdress = "http://lb-fall-detection-api-3856896.us-west-2.elb.amazonaws.com/predict";
                URL obj = new URL(urlAdress);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                con.setRequestProperty("Accept", "application/json");
                con.setDoOutput(true);

                OutputStream os = con.getOutputStream();
                os.write(result.getBytes());
                os.flush();
                os.close();
                int responseCode = con.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK)
                {
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();

                    res.setText(response.toString());
                    //System.out.println(response.toString());
                    String tmpStr = String.valueOf(response);
                    String newStr = tmpStr.substring(1, tmpStr.length()-1);
                    String jsonStr = newStr;
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String name = jsonObj.getString("prediction");
                    if(name.equals("Fall Detected"))
                    {
                        System.out.println(name);

                        //openDialog(name);
                    }
                }
            }
            catch (Exception e)
            {
                //e.printStackTrace();
            }
        }
    }



    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
