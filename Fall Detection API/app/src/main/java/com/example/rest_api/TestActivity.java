package com.example.rest_api;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class TestActivity extends AppCompatActivity {

    Button btn;
    int i = 0;
    int j;
    TextView sensor_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        btn = findViewById(R.id.fallDetection);
        sensor_data = findViewById(R.id.sensor_data);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runThread();
                getData();
            }
        });
    }

    public void getData()
    {
        new Thread()
        {
            public void run()
            {
                while(j++<1000)
                {
                    try
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //System.out.println("Thread 2 :- Capture the data and provide it to the api for detection");
                            }
                        });
                        Thread.sleep(100);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    private void runThread() {

        new Thread() {
            public void run() {
                while (i++ < 1000) {
                    try {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                //System.out.println("Thread 1 :- ");
                                btn.setText("#" + i);
                            }
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}
